#include <stdio.h>
#include <stdlib.h>

int main() {
    int tamanio;
    int i;
    int *puntero;
    printf("Ingrese el tamaño del vector: ");
    scanf("%d", &tamanio);
    int v[tamanio];
    printf("\nIngrese los valores del vector:\n");
    for (i = 0; i < tamanio; i++) {
        printf("posicion %d: ", i + 1);
        scanf("%d", &v[i]);
    }
    puntero = &v;

    printf("\nVector de la manera ingresada:\n[");
    for (i = 0; i < tamanio; i++) {
        printf(" %d ", *puntero);
        puntero++;
    }
    printf("]");
/*
    
    for (i = 0; i < tamanio; i++) {
        printf("\nDIRECCIONES DE MEMORIA:");
        printf("\nDireccion: %p",puntero);
    }
*/
     for (i = 0; i<tamanio; i++) {
        printf("%d  %p\n", *puntero, puntero);
    }
    return 0;
}


